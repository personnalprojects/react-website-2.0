import { render, screen } from '@testing-library/react';
import App from './App';
import defaultLang from './langs/index.js';

test('Renders navbar links correctly', () => {
  render(<App />);
  const linksArray = [];
  if(defaultLang === 'fr'){
    const homeLink = screen.getByText(/Accueil/i);
    const servicesLink = screen.getByText(/Mes services/i);
    const workLink = screen.getByText(/Mon travail/i);
    const quotesLink = screen.getByText(/Devis/i);
    const blogLink = screen.getByText(/Mon blog/i);
    const contactLink = screen.getByText(/Me contacter/i);

    linksArray.push(homeLink, servicesLink, workLink, quotesLink, blogLink, contactLink);
  }
  
  for(let i = 0; i < linksArray.length; i++){
    expect(linksArray[i]).toBeInTheDocument();
  }
});
