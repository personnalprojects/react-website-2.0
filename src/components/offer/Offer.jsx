import React from 'react';
import './offer.scss';

function Offer(props){

  const goToQuote = () => {
    // TODO Prefill quote request
    window.location.href = '#quotes';
  }

  return(
    <div className={`offer d-flex flex-column align-items-center justify-content-between ms-2 me-3`}>
      <div className="col-12 d-flex flex-column align-items-center">
        <p className="offer-title m-0 mt-2">{props.title ? props.title : "Erreur"}</p>
        <p className="offer-text px-3 text-center">{props.description ? props.description : "Pas de description"}</p>
        <div className="offer-separator"></div>
        {
          props.products.map((product, index) => {
            return(
              <div key={index} className="col-12 d-flex flex-column align-items-center">
                <div className="col-12 px-3">
                  <p className="offer-product m-0 my-2">{product}</p>
                </div>
                <div className="offer-separator"></div>
              </div>
            )
          })
        }
      </div>
      <div className="col-12 px-3 d-flex flex-row justify-content-between">
        <div className="d-flex flex-column justify-content-center">
          <p className="m-0">À partir de</p>
          <p className="offer-price m-0">{props.price} €<span className="offer-ttc">ttc*</span> {props.annual ===true ? <span className="offer-annual">/an</span> : ""}</p>
        </div>
        <div className="d-flex flex-row align-items-end justify-content-center">
          <img onClick={() => goToQuote()} className="offer-more-button mb-3" src="./img/plus.png" alt="Button"/>
        </div>
      </div>
    </div>
  )
}

export default Offer;
