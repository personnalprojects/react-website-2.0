import React, {useEffect} from 'react';
import AOS from 'aos';
import './navbar.scss';

function Navbar(props){
  useEffect(() => {
    AOS.init();
  })

  let hasPassed = false;
  const navbarTest = () => {
    // let homeRect = document.getElementById('home-content').getBoundingClientRect();
    // if(window.scrollY > homeRect.top + (homeRect.height/3)){
    //   if(!hasPassed){
    //     console.log("Setting new bg for navbar");
        // document.getElementById('navbar').style.backgroundImage = 'url("./img/wave_navbar.svg")';
        // document.getElementById('navbar-background').classList.replace('d-none', 'd-block');
    //     document.getElementById('wave').classList.add('d-none');
    //     hasPassed = !hasPassed;
    //   }else{
    //     console.log("resetting bg for navbar");
    //     document.getElementById('wave').classList.replace('d-none', 'd-block');
    //     hasPassed = !hasPassed;
    //   }
    // }
  }
  document.addEventListener('scroll', navbarTest, {once: true});

  return(
    <div id="navbar" className="navbar col-12 d-flex flex-row justify-content-between sticky-top">
      <svg viewBox="0 0 500 150" data-aos="fade-down" data-aos-anchor="#about" data-aos-duration="300" preserveAspectRatio="none" style={{position: "absolute", height: "25vh", width: "70vw"}}><path d="M-5.92,136.67 C112.58,7.41 426.35,252.13 509.31,-18.25 L508.74,-33.05 L0.00,0.00 Z" style={{stroke: "none", fill:"#9146FF"}}></path></svg>
      {/* <img id="navbar-background" data-aos="fade-down" data-aos-anchor="#test" data-aos-duration="300" src="./img/wave_navbar.svg" alt="Navbar Background"/> */}
      <div data-aos-delay="1550" data-aos-once="true" data-aos="fade-in" className="d-flex flex-row">
        <li className="navbar-link ms-5 me-5"><a href="/#home">Accueil</a></li>
        <li className="navbar-link me-5"><a href="/#about">À propos</a></li>
        <li className="navbar-link me-5"><a href="/#services">Mes services</a></li>
        <li className="navbar-link me-5"><a href="/#projects">Projets</a></li>
        <li className="navbar-link me-5"><a href="/#quote">Devis</a></li>
        <li className="navbar-link me-5"><a href="/contact">Me contacter</a></li>
      </div>
      <div className="d-flex flex-row">
        <a href="/"><img className="navbar-img pe-3" src="./img/logos/ea.svg" alt="Logo Enzo Avagliano"/></a>
      </div>
    </div>
  )
}

export default Navbar;
