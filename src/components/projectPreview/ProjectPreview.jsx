import React from 'react';
import './projectPreview.scss';

function ProjectPreview(props){
  return(
    <div className="project mb-3">
      <img src="./img/test.jpg" alt=""/>
      <div className="project-infos d-flex flex-column justify-content-end p-3">
        <p className="m-0 project-title">{props.title}</p>
        <p className="m-0 project-short-description">{props.short}</p>
      </div>
    </div>
  )
}

export default ProjectPreview;
