import React, {useEffect} from 'react';
import AOS from 'aos';
import lang from '../../langs/modules/LangSelector';
import Offer from '../../components/offer/Offer';
import ProjectPreview from '../../components/projectPreview/ProjectPreview';
import './home.scss';

function Home(){
  useEffect(() => {
    AOS.init({duration:1000});
    document.getElementById('wave').style.transform = "scale(1, 1)";
  })

  return(
    <div id="home">
      <img id="wave" data-aos-once data-aos="zoom-in-right" src="./img/wave.svg" alt=""/>
      {/* <img id="navbar-background" className="sticky-top" data-aos="fade-down" data-aos-anchor="#test" data-aos-duration="300" src="./img/wave_navbar.svg" alt="Navbar Background"/> */}

      <div id="home-content" className="home-content d-flex flex-column align-items-end justify-content-center">
        <div className="title-container text-end">
          <p data-aos-once data-aos="fade-left" className="title m-0">{lang.landpage.title}</p>
          <p data-aos-once data-aos="fade-left" className="subtitle m-0">{lang.landpage.subtitle[0]}<br/>{lang.landpage.subtitle[1]}</p>
          <p data-aos-once data-aos="fade-left" className="catch-phrase m-0 mt-4">{lang.landpage.catchphrase}</p>
        </div>
        <a id="cta" className="cta" href="#devis">{lang.landpage.cta}</a>
      </div>
      <div id="about" className="about d-flex flex-column">
        <p className="section-title">{lang.about.title}</p>
        <div className="col-12 d-flex flex-column flex-lg-row mb-5">
          <div className="col-12 col-lg-8">
            <p className="section-text mb-3">{lang.about.text[0]}</p>
            <p className="section-text ">{lang.about.text[1]}</p>
          </div>
          <div className="profile-picture-container col-12 col-lg-4 d-flex flex-column align-items-center justify-content-center">
            <img className="blob" src="./img/blob.svg" alt="Blob"/>
            <img className="profil-picture" src="./img/pp.jpg" alt="Enzo Avagliano"/>
          </div>
        </div>
        <div className="mt-3">
          <p className="section-text" style={{fontWeight:'600'}}>{lang.about.enterprises}</p>
          <div className="d-flex flex-column flex-lg-row">
            <img className="trust-logo me-5" src="./img/logos/logo-archee.png" alt="Logo Archee"/>
            <img className="trust-logo ms-5" src="./img/logos/logo-visiopm.png" alt="Logo VisioPM"/>
          </div>
        </div>
      </div>

      <div id="services" className="services mb-5">
        <p className="section-title">{lang.services.title}</p>
        <div className="offers-container col-12 d-flex flex-wrap flex-lg-row justify-content-center">
          {
            lang.services.offers.map((offer, index) => {
              return(
                <Offer title={offer.title} description={offer.description} products={offer.products} price={offer.price} annual={offer.annual}/>
              )
            })
          }
        </div>
        <div className="col-12 mt-2 text-center">
          <p className="m-0 offers-disclaimer">* Les prix affichés sont des prix de référence et sont suceptibles de varier d'un devis à un autre. - ** Le pack maintenance ne nécéssite pas d'être déjà client.</p>
        </div>
      </div>

      <div id="projects" className="projects">
        <p className="section-title m-0">{lang.projects.title}</p>
        <p className="section-text m-0">{lang.projects.subtitle}</p>

        <div className="projects-container col-12 h-100 d-flex flex-row align-items-center mt-5">
          {
            lang.projects.projects.map((project, index) => {
              return(
                <ProjectPreview key={index} title={project.title} short={project.short_description}/>
              )
            })
          }
        </div>
      </div>

      <div id="quote">
        <p className="section-title">{lang.quote.title}</p>
      </div>
    </div>
  )
}

export default Home;
