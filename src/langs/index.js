const langs = [
  'fr',
  'en'
];
const defaultLang = langs[0];

export default defaultLang;
