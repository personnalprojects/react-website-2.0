import frNavbarTranslation from './navbar/index.js';
import frHomeTranslation from './home/index.js';
import frGlobalTranslation from './global/index.js';

const frTranslation = Object.assign({},
  frNavbarTranslation,
  frHomeTranslation,
  frGlobalTranslation
);

export default frTranslation;
