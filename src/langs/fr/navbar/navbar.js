module.exports = {
  navbar: {
    titles: {
      home: "Accueil",
      services: "Mes services",
      work: "Mon travail",
      quote: "Devis",
      blog: "Mon blog",
      contact: "Me contacter"
    },
    links: {
      home: "/home",
      services: "/home#services",
      work: "/home#work",
      quote: "/home#quotes",
      blog: "https://blog.enzoavagliano.fr",
      contact: "/contact"
    }
  }
}
