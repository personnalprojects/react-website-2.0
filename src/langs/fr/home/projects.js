module.exports = {
  projects: {
    title: "Projets",
    subtitle: "Voici un aperçu des différents projet que j’ai pu réaliser lors de mes expériences professionnelles",
    projects: [
      {
        title: "Projet 1",
        short_description: "A short description",
        long_description: "A much much longer description with more detils about the project and its technologies.",
        project_miniature: "./img/project/image_minia.png",
        images: [
          "./img/projects/image_1.png",
          "./img/projects/image_2.png",
          "./img/projects/image_3.png",
          "./img/projects/image_4.png",
          "./img/projects/image_5.png"
        ],
        links: [
          {
            title: "gitlab",
            link: "linktogitlab.html"
          },
          {
            title: "prod",
            link: "linktowebsite.html"
          }
        ]
      },
      {
        title: "Projet 2",
        short_description: "A short description",
        long_description: "A much much longer description with more detils about the project and its technologies.",
        project_miniature: "./img/project/image_minia.png",
        images: [
          "./img/projects/image_1.png",
          "./img/projects/image_2.png",
          "./img/projects/image_3.png",
          "./img/projects/image_4.png",
          "./img/projects/image_5.png"
        ],
        links: [
          {
            title: "gitlab",
            link: "linktogitlab.html"
          },
          {
            title: "prod",
            link: "linktowebsite.html"
          }
        ]
      },
      {
        title: "Projet 3",
        short_description: "A short description",
        long_description: "A much much longer description with more detils about the project and its technologies.",
        project_miniature: "./img/project/image_minia.png",
        images: [
          "./img/projects/image_1.png",
          "./img/projects/image_2.png",
          "./img/projects/image_3.png",
          "./img/projects/image_4.png",
          "./img/projects/image_5.png"
        ],
        links: [
          {
            title: "gitlab",
            link: "linktogitlab.html"
          },
          {
            title: "prod",
            link: "linktowebsite.html"
          }
        ]
      },
      {
        title: "Projet 4",
        short_description: "A short description",
        long_description: "A much much longer description with more detils about the project and its technologies.",
        project_miniature: "./img/project/image_minia.png",
        images: [
          "./img/projects/image_1.png",
          "./img/projects/image_2.png",
          "./img/projects/image_3.png",
          "./img/projects/image_4.png",
          "./img/projects/image_5.png"
        ],
        links: [
          {
            title: "gitlab",
            link: "linktogitlab.html"
          },
          {
            title: "prod",
            link: "linktowebsite.html"
          }
        ]
      }
    ]
  }
}
