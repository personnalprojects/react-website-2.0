import frLandpage from './landpage';
import frAbout from './about.js';
import frServices from './services';
import frProjects from './projects';
import frQuote from './quote';

const frHomeTranslation = Object.assign({},
  frLandpage,
  frAbout,
  frServices,
  frProjects,
  frQuote,
);

export default frHomeTranslation;
