module.exports = {
  services: {
    title: "Mes services",
    offers: [
      {
        title: "Starter",
        description: "Développement de votre site vitrine avec l'outil Wordpress. Le moyen le plus simple de vous faire connaître sur le web.",
        products: [
          "Utilisation de votre charte graphique",
          "Votre site responsive",
          "Votre nom de domaine + votre hébergement",
          "Votre contenu et vos images",
          "Référencement optimal"
        ],
        price: "1200",
        annual: false
      },
      {
        title: "Custom",
        description: "Développement de votre site vitrine sur-mesure (codé à la main) vous offrant une plus ample personnalisation.",
        products: [
          "Utilisation de votre charte graphique",
          "Votre site responsive",
          "Votre nom de domaine + votre hébergement",
          "Votre contenu et vos images",
          "Référencement optimal",
          "Espace administrateur (en option)",
          "Statistiques en temps réel (en option)",
          "Maintenance incluse pendant 6 mois"
        ],
        price: "1500",
        annual: false
      },
      {
        title: "E-Commerce",
        description: "Votre site E-commerce développé sous Wordpress. Attirez une nouvelle clientèle avec votre catalogue en ligne !",
        products: [
          "Utilisation de votre charte graphique",
          "Votre site responsive",
          "Votre nom de domaine + votre hébergement",
          "Votre contenu et vos images",
          "Référencement optimal",
          "Paiements en ligne",
          "Espace administrateur",
          "Statistiques en temps réel",
          "Maintenance incluse pendant 6 mois"
        ],
        price: "2500",
        annual: false
      },
      {
        title: "Maintenance",
        description: "Le pack maintenance vous offre la possibilité de faire évoluer votre site dans le temps, sans avoir à vous soucier des détails.",
        products: [
          "Mise a jour de contenu",
          "Modifications de visuels (légères)",
          "Ajouts d'options",
          "Gestion nom de domaine & hébergement"
        ],
        price: "200",
        annual: true
      },
    ]
  }
}
