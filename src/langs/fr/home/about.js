module.exports = {
  about: {
    title: "À propos",
    text: [
      "Je suis Enzo Avagliano, développeur Web basé à Aix en Provence. Je vous accompagne dans la conception et le développment de tous vos projets Web : site vitrine, site e-commerçe, application web, refonte et optimisation de sites.",
      "Je propose des services de développement adaptés à vos besoins, avec un suivi et une communication accrue afin de vous apporter la meilleure solution possible."
    ],
    enterprises: "Ces entreprises m'ont déjà fait confiance :"
  }
}
