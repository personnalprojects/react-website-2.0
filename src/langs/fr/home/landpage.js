module.exports = {
  landpage: {
    title: "Enzo Avagliano",
    subtitle: [
      "Création de sites internet",
      "et d'applications web"
    ],
    catchphrase: "Des solutions sur-mesure et adaptées à vos besoins.",
    cta: "Vous avez un projet ?"
  }
}
