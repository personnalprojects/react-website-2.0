import frGlobal from './global';

const frGlobalTranslation = Object.assign({},
  frGlobal,
);

export default frGlobalTranslation;
