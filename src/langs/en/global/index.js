import enGlobal from './global';

const enGlobalTranslation = Object.assign({},
  enGlobal,
);

export default enGlobalTranslation;
