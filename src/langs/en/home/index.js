import enLandpage from './landpage';
import enServices from './services';

const enHomeTranslation = Object.assign({},
  enLandpage,
  enServices
);

export default enHomeTranslation;
