import enNavbarTranslation from './navbar/index.js';
import enHomeTranslation from './home/index.js';
import enGlobalTranslation from './global/index.js';

const enTranslation = Object.assign({},
  enNavbarTranslation,
  enHomeTranslation,
  enGlobalTranslation
);

export default enTranslation;
