module.exports = {
  navbar: {
    titles: {
      home: "Home",
      services: "My services",
      work: "My work",
      quote: "Quotes",
      blog: "My blog",
      contact: "Contact me"
    },
    links: {
      home: "/home",
      services: "/home#services",
      work: "/home#work",
      quote: "/home#quotes",
      blog: "https://blog.enzoavagliano.fr",
      contact: "/contact"
    }
  }
}
