import frTranslation from '../../langs/fr/index.js';
import enTranslation from '../../langs/en/index.js';
import defaultLang from '../../langs/index.js';

  let lang;
  switch(defaultLang){
    case 'fr':
    lang = frTranslation;
    break;
    case 'en':
    lang = enTranslation;
    break;
    default: lang = frTranslation;
  }

export default lang;
