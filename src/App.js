import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Navbar from './components/navbar/Navbar';
import './main.scss';

import Home from './pages/home/Home';

function App() {
  return (
    <div id="app">
      <Router>
        <Navbar />
        <Switch>
          <Route exact path="/" component={props =>
            <div>
              <Home/>
            </div>
          }/>

          <Route path="/home" component={props =>
            <div>
              <Home/>
            </div>
          }/>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
